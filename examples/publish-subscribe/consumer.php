<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__.'/../../vendor/autoload.php';

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->exchange_declare('event', 'fanout', false, false, false);
[$queueName] = $channel->queue_declare();
$channel->queue_bind($queueName, 'event');

try {
    $callback = static function (AMQPMessage $message) {
        echo sprintf("[%s] Received %s\n", (new \DateTime())->format('Y-m-d H:i:s.u'), $message->body);
        $message->ack();
    };

    $channel->basic_consume($queueName, '', false, false, false, false, $callback);
    $channel->consume();
} catch (\Throwable $exception) {
    echo $exception->getMessage();
}

$channel->close();
$connection->close();
