<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__.'/../../vendor/autoload.php';

$animal = $argv[1] ?? '';
if ($animal === '') {
    echo sprintf("Usage: %s (dog|cat)\n", $argv[0]);
    exit(1);
}

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->exchange_declare('animals', 'direct', false, false, false);
[$queueName] = $channel->queue_declare('', false, false, true, false);
$channel->queue_bind($queueName, 'animals', $animal);

try {
    $callback = static function (AMQPMessage $message) {
        echo sprintf("[%s] Received %s\n", (new \DateTime())->format('Y-m-d H:i:s.u'), $message->body);
    };

    $channel->basic_consume($queueName, '', false, true, false, false, $callback);
    $channel->consume();
} catch (\Throwable $exception) {
    echo $exception->getMessage();
}

$channel->close();
$connection->close();
