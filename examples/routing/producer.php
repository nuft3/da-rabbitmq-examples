<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__.'/../../vendor/autoload.php';

$animals = ['dog', 'cat'];

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->exchange_declare('animals', 'direct', false, false, false);

for ($i = 0; $i < 100; ++$i) {
    $animal = $animals[random_int(0, count($animals) - 1)];
    $message = new AMQPMessage(json_encode([$animal, str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ')], JSON_THROW_ON_ERROR));
    $channel->basic_publish($message, 'animals', $animal);
}

$channel->close();
$connection->close();
