<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__.'/../../vendor/autoload.php';

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->basic_qos(0, 1, false);
$channel->queue_declare('rpc', false, false, false, false);

$callback = static function (AMQPMessage $requestMessage) {
    echo sprintf("[%s] Received %s\n", (new \DateTime())->format('Y-m-d H:i:s.u'), $requestMessage->body);

    [$method, $parameters] = json_decode($requestMessage->body, true, 512, JSON_THROW_ON_ERROR);
    $result = call_user_func_array($method, $parameters);

    $responseMessage = new AMQPMessage((string) $result, ['correlation_id' => $requestMessage->get('correlation_id')]);
    $requestMessage->getChannel()->basic_publish($responseMessage, '', $requestMessage->get('reply_to'));
    $requestMessage->ack();
};
$channel->basic_consume('rpc', '', false, false, false, false, $callback);

try {
    $channel->consume();
} catch (\Throwable $exception) {
    echo $exception->getMessage();
}

$channel->close();
$connection->close();

function rpcAdd($first, $second)
{
    return $first + $second;
}
