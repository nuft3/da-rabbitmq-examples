<?php

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__.'/../../vendor/autoload.php';

class RpcClient
{
    private AMQPStreamConnection $connection;
    private AMQPChannel $channel;
    private string $callbackQueue;
    private ?AMQPMessage $response = null;
    private ?string $correlation_id = null;

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $this->channel = $this->connection->channel();
        [$this->callbackQueue] = $this->channel->queue_declare('', false, false, true, false);
        $this->channel->basic_consume(queue: $this->callbackQueue, no_ack: true, callback: [$this, 'onResponse']);
    }

    public function onResponse(AMQPMessage $response): void
    {
        if ($response->get('correlation_id') === $this->correlation_id) {
            $this->response = $response;
        }
    }

    public function call(string $method, array $parameters): string
    {
        $this->response = null;
        $this->correlation_id = uniqid('', true);

        $message = new AMQPMessage(
            json_encode([$method, $parameters], JSON_THROW_ON_ERROR),
            [
                'correlation_id' => $this->correlation_id,
                'reply_to' => $this->callbackQueue,
            ]
        );
        $this->channel->basic_publish($message, '', 'rpc');
        while (!$this->response) {
            $this->channel->wait();
        }

        return $this->response->body;
    }
}

$client = new RpcClient();
for ($i = 0; $i < 100; ++$i) {
    $result = $client->call('rpcAdd', [random_int(0, 99), random_int(0, 99)]);
    echo sprintf("[%s] Got %s\n", (new \DateTime())->format('Y-m-d H:i:s.u'), $result);
}
