<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__.'/../../vendor/autoload.php';

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->queue_declare('simple', false, false, false, false);

try {
    $callback = static function (AMQPMessage $message) {
        echo sprintf("[%s] Received %s\n", (new \DateTime())->format('Y-m-d H:i:s.u'), $message->body);
    };

    $channel->basic_consume('simple', '', false, true, false, false, $callback);
    $channel->consume();
} catch (\Throwable $exception) {
    echo $exception->getMessage();
}

$channel->close();
$connection->close();
