<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__.'/../../vendor/autoload.php';

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->queue_declare('simple', false, false, false, false);

for ($i = 0; $i < 100; $i++) {
    $message = new AMQPMessage(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'));
    $channel->basic_publish($message, '','simple');
}

$channel->close();
$connection->close();
